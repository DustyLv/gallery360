﻿Shader "Unlit/UnlitTextureOverlay"
{
	Properties
	{
		_MainTex ("Base Texture", 2D) = "white" {}
		_OverlayTex("Overlay Texture", 2D) = "white" {}
		_OverlayColor("Overlay Color", Color) = (1,1,1,1)
		_OverlayAlpha("Overlay Alpha", Range(0.0,1.0)) = 1.0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			//Blend One One
			//AlphaToMask On
			//Blend SrcAlpha OneMinusSrcAlpha
			//Blend OneMinusDstColor One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			
			

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			fixed4 _MainTex_ST;

			sampler2D _OverlayTex;
			fixed4 _OverlayTex_ST;

			fixed4 _OverlayColor;

			fixed _OverlayAlpha;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 overlay = tex2D(_OverlayTex, i.uv);

				//overlay.a = _OverlayAlpha;
				overlay = overlay * _OverlayColor;

				fixed4 texOut = lerp(col.rgba, overlay.rgba, overlay.a * _OverlayAlpha);

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return texOut;
			}
			ENDCG
		}
	}
}
