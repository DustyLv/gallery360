﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageMap : MonoBehaviour {

    public Text textbox;
    public float rayDistance = 50f;
    public LayerMask layer;
    public Texture2D clickTexture;

    public RoomChanger roomChanger;
    public SampleGame game;

    public Hotspot[] hotspots;

    public Texture2D cursorNormal;
    public Texture2D cursorInteractive;

    //public Color[] colors;
    //public string[] texts;

    private float targetTime = 0.1f;
    private float defaultTargetTime = 0.1f;
    private bool cursorIsNormal = true;

	// Update is called once per frame
	void FixedUpdate () {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, rayDistance, layer))
        {
            Renderer renderer = hit.transform.GetComponent<MeshRenderer>();
            Texture2D texture = renderer.material.mainTexture as Texture2D;
            Vector2 pixelUV = hit.textureCoord;
            pixelUV.x *= texture.width;
            pixelUV.y *= texture.height;
            Vector2 tiling = renderer.material.mainTextureScale;
            Color color = clickTexture.GetPixel(Mathf.FloorToInt(pixelUV.x * tiling.x), Mathf.FloorToInt(pixelUV.y * tiling.y));

            int index = FindIndexFromColor(color);

            if (index >= 0)
            {
                Cursor.SetCursor(cursorInteractive, Vector2.zero, CursorMode.ForceSoftware);

                if (Input.GetMouseButtonDown(0))
                {
                    if (hotspots[index].isDoor)
                    {
                        roomChanger.ChangeRoom(gameObject, hotspots[index].doorTarget);
                        textbox.text = "";
                    }else if (hotspots[index].isPointObject)
                    {
                        game.AddPoints(1);
                        textbox.text = hotspots[index].text + " atrasts!";
                    }
                    else
                    {
                        textbox.text = hotspots[index].text;
                    }
                }

            }else
            {
                Cursor.SetCursor(cursorNormal, Vector2.zero, CursorMode.ForceSoftware);
            }

        }
    }

    private int FindIndexFromColor(Color color)
    {
        for (int i = 0; i < hotspots.Length; i++)
        {
            if (hotspots[i].color == color)
            {
                return i;
            }
        }
        return -1;
    }

    //private int FindIndexFromColor(Color color)
    //{
    //    for (int i = 0; i < colors.Length; i++)
    //    {
    //        if (colors[i] == color)
    //        {
                
    //            return i;
    //        }
    //    }
    //    return -1;
    //}


}

[System.Serializable]
public class Hotspot
{
    public string name;
    public Color color;
    public string text;
    public bool isDoor;
    public bool isPointObject;
    public GameObject doorTarget;
}
