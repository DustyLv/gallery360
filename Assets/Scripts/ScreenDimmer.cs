﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenDimmer : MonoBehaviour {

    public CanvasGroup dimmer;
    public float speed = 1f;
    public float screenBlackTimeLength = 1f;

    private WaitForSeconds screenBlackTime;

	void Start () {
        dimmer.alpha = 0;
        screenBlackTime = new WaitForSeconds(screenBlackTimeLength);
	}
	
    public void DimScreen()
    {
        StopCoroutine("Dim");
        StartCoroutine("Dim");
    }

    IEnumerator Dim()
    {
        //dimmer.alpha = 0;
        while(dimmer.alpha < 1)
        {
            dimmer.alpha += speed * Time.deltaTime;
            yield return null;
        }
        //dimmer.alpha = 1;
        yield return screenBlackTime;

        while (dimmer.alpha > 0)
        {

            dimmer.alpha -= speed * Time.deltaTime;
            yield return null;
        }
        dimmer.alpha = 0;
    }
}
