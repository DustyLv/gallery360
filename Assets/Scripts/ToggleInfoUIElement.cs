﻿using UnityEngine;
using System.Collections;

public class ToggleInfoUIElement : MonoBehaviour {

    private bool state = false;

    public GameObject targetUI;

    public void Toggle(GameObject target)
    {
        state = !state;
        target.SetActive(state);
    }

    public void Toggle()
    {
        state = !state;
        targetUI.SetActive(state);
    }
}
