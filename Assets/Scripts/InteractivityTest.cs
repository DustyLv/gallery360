﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InteractivityTest : MonoBehaviour {

    public Text outPutTextfield;

    public void TextOut(string text)
    {
        outPutTextfield.text = text;
        StopCoroutine("DelayedTextClear");
        StartCoroutine("DelayedTextClear");
    }

    IEnumerator DelayedTextClear()
    {
        yield return new WaitForSeconds(10f);
        outPutTextfield.text = "";
    }

}
