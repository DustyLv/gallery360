﻿using UnityEngine;
using System.Collections;

public class RoomChanger : MonoBehaviour {

    public GameObject toRoom;

    private ScreenDimmer screenDimmer;

    void Start()
    {
        screenDimmer = GameObject.FindObjectOfType<ScreenDimmer>();
    }

    public void ChangeRoom(GameObject to)
    {
        DimScreen();
        transform.root.gameObject.SetActive(false);
        to.SetActive(true);
    }

    public void ChangeRoom(GameObject from, GameObject to)
    {
        DimScreen();
        from.SetActive(false);
        to.SetActive(true);
    }

    public void ChangeRoom()
    {
        DimScreen();
        transform.root.gameObject.SetActive(false);
        toRoom.SetActive(true);
    }

    private void DimScreen()
    {
        if(screenDimmer != null)
        {
            screenDimmer.DimScreen();
        }else
        {
            print("fucking hell, cant find this screen dimmer shit!");
        }
    }
}
