﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleGame : MonoBehaviour {

    public int points = 0;

    public void AddPoints(int amount)
    {
        points += amount;
    }
}
