﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MouseLook : MonoBehaviour {

    public float sensitivity = 5f;
    public float zoomSensitivity = 5f;
    public float defaultZoom = 60;

    public float vMin;
    public float vMax;

    public Slider zoomSlider;

    private Transform mTransform;
    private Camera cam;
    private float v;
    private float h;
    private float zoom;

	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
        zoom = defaultZoom;
        cam.fieldOfView = zoom;
	}
	
	// Update is called once per frame
	void Update () {

        //if(Application.platform == RuntimePlatform.Android)
        //{
        //    var touch = Input.GetTouch(0);
        //    if (touch.phase == TouchPhase.Moved)
        //    {
        //        Vector2 touchDelta = touch.deltaPosition;
        //        v += touchDelta.y;
        //        h += touchDelta.x;

        //        v = ClampAngle(v, vMin, vMax);
        //    }
        //}

        //if(Application.platform == RuntimePlatform.WebGLPlayer || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        //{
 
        //}

        if (Input.GetButton("Fire1"))
        {

            if (!EventSystem.current.IsPointerOverGameObject())
            {
                v += Input.GetAxis("Mouse Y");
                h += Input.GetAxis("Mouse X");

                v = ClampAngle(v, vMin, vMax);
            }


        }

        transform.eulerAngles = new Vector3(v * sensitivity, -h * sensitivity, 0);

        if(Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            zoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
            zoom = Mathf.Clamp(zoom, 20f, 90f);
            cam.fieldOfView = zoom;
        }
    }

    float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    public void Zoom(float val)
    {
        cam.fieldOfView = val;
    }

    public void ResetZoom()
    {
        cam.fieldOfView = defaultZoom;
        zoomSlider.value = cam.fieldOfView;
    }
}
