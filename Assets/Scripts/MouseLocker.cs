﻿using UnityEngine;
using System.Collections;

public class MouseLocker : MonoBehaviour {

    public bool mouseGo = false;

	void Start () {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
	}
	
	void Update () {
        if (Input.GetButtonDown("Fire2"))
        {
            mouseGo = !mouseGo;
            ToggleCursor();
        }
	}

    void ToggleCursor()
    {
        if (mouseGo)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }
}
